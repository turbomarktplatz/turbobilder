#!/usr/bin/env sh

case $TARGETARCH in
    "amd64")
        echo "x86_64-unknown-linux-musl" > /.platform
        echo "" > /.compiler 
        ;;
    "arm64") 
        echo "aarch64-unknown-linux-musl" > /.platform
        echo "gcc-aarch64-linux-gnu" > /.compiler
        ;;
    *)
        echo "The target $TARGETARCH is unsuported."
        exit -1
esac

