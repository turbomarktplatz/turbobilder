FROM --platform=$BUILDPLATFORM rust:1.77 AS builder

ENV BIN_NAME turbobilder
ENV CARGO_TARGET_AARCH64_UNKNOWN_LINUX_MUSL_LINKER aarch64-linux-gnu-gcc

ARG TARGETARCH
COPY ./CI/platform.sh .
RUN ./platform.sh

RUN apt-get update && apt-get install -y musl musl-tools $(cat /.compiler)
RUN rustup target add $(cat /.platform)

WORKDIR /app

RUN cargo init --vcs none
COPY Cargo.toml Cargo.lock /app/

RUN --mount=type=cache,target=/usr/local/cargo/registry \
  cargo build --target $(cat /.platform) --release

COPY src /app/src

RUN --mount=type=cache,target=/usr/local/cargo/registry <<EOF
  set -e
  # update timestamps to force a new build
  echo "" >> /app/src/main.rs

  cargo build --target $(cat /.platform) --release
  mv target/$(cat /.platform)/release/$BIN_NAME server
EOF



FROM --platform=$TARGETARCH alpine 

WORKDIR /app

COPY --from=builder /app/server .

EXPOSE 3002

CMD ["./server"]
