/*
* Copyright (c) 2023 Daniél Kerkmann <daniel@kerkmann.dev>
*
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by
* the European Commission - subsequent versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the Licence is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and
* limitations under the Licence.
*
* SPDX-License-Identifier: EUPL-1.2
*/

use std::{io::Cursor, mem::size_of_val};

use aws_sdk_s3::{
    error::ProvideErrorMetadata, operation::get_object::GetObjectError, primitives::ByteStreamError,
};
use axum::{
    extract::{Path, Query, State},
    http::{header, StatusCode},
    response::IntoResponse,
    routing::get,
    Router,
};
use image::{
    codecs::webp::WebPEncoder, imageops::FilterType, io::Reader as ImageReader, ExtendedColorType,
    ImageError, ImageFormat,
};
use serde::Deserialize;
use serde_with::{serde_as, DisplayFromStr};
use snafu::{prelude::*, Location, Report};
use tower_http::compression::CompressionLayer;
use tracing::Level;
use tracing_subscriber::{layer::SubscriberExt, util::SubscriberInitExt, EnvFilter};

#[derive(Debug, Clone)]
struct AppData {
    config: Config,
    s3_client: aws_sdk_s3::Client,
}

#[derive(Debug, Clone, Deserialize)]
#[serde(rename_all = "SCREAMING_SNAKE_CASE")]
struct Config {
    #[allow(dead_code)]
    aws_access_key_id: String,
    #[allow(dead_code)]
    aws_secret_access_key: String,
    #[allow(dead_code)]
    aws_default_region: String,
    aws_endpoint_url: String,

    allowed_hosts: Vec<String>,

    #[serde(default = "default_allowed_sizes")]
    allowed_sizes: Vec<u32>,

    #[serde(default = "default_allowed_crops")]
    allowed_crops: Vec<CropParameter>,

    #[serde(default = "default_allow_webp")]
    allow_webp: bool,

    #[serde(default = "default_address")]
    address: String,

    #[serde(default = "default_port")]
    port: u32,

    #[serde(default = "default_images_path")]
    images_path: String,
}

fn default_allowed_sizes() -> Vec<u32> {
    vec![]
}

fn default_allowed_crops() -> Vec<CropParameter> {
    vec![]
}

fn default_allow_webp() -> bool {
    false
}

fn default_address() -> String {
    "0.0.0.0".to_string()
}

fn default_port() -> u32 {
    3002
}

fn default_images_path() -> String {
    "./images".to_string()
}

#[derive(Debug, Clone, Deserialize)]
struct Parameters {
    size: Option<u32>,
    #[serde(flatten)]
    crop: Option<CropParameter>,
    #[serde(flatten)]
    webp_encoder: Option<WebPParameter>,
}

#[serde_as]
#[derive(Debug, Clone, PartialEq, Deserialize)]
struct CropParameter {
    #[serde_as(as = "DisplayFromStr")]
    x: u32,
    #[serde_as(as = "DisplayFromStr")]
    y: u32,
    #[serde_as(as = "DisplayFromStr")]
    width: u32,
    #[serde_as(as = "DisplayFromStr")]
    height: u32,
}

#[derive(Debug, Clone, PartialEq, Deserialize)]
enum Format {
    WebP,
}

#[serde_as]
#[derive(Debug, Clone, PartialEq, Deserialize)]
struct WebPParameter {
    #[serde_as(as = "DisplayFromStr")]
    quality: i32,
}

impl std::fmt::Display for Format {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::WebP => f.write_str("webp"),
        }
    }
}

#[derive(Debug, Snafu)]
enum Error {
    #[snafu(display("{source:?} {location:?}"), context(false))]
    IoError {
        source: std::io::Error,

        #[snafu(implicit)]
        location: Location,
    },

    #[snafu(display("{source:?} {location:?}"), context(false))]
    ReqwestError {
        source: reqwest::Error,

        #[snafu(implicit)]
        location: Location,
    },

    #[snafu(display("{source:?} {location:?}"), context(false))]
    ImageError {
        source: ImageError,

        #[snafu(implicit)]
        location: Location,
    },

    #[snafu(display("{source:?} {location:?}"), context(false))]
    ByteStreamError {
        source: ByteStreamError,

        #[snafu(implicit)]
        location: Location,
    },

    #[snafu(display("{source:?} {location:?}"), context(false))]
    GetObjectError {
        source: GetObjectError,

        #[snafu(implicit)]
        location: Location,
    },

    #[snafu(display("{key}: {size:?}, {message}"))]
    PutObject2 {
        key: String,
        size: usize,
        message: String,
    },

    #[snafu(whatever, display("{message} {location:?}"))]
    GenericError {
        message: String,

        #[snafu(source(from(Box<dyn std::error::Error>, Some)))]
        source: Option<Box<dyn std::error::Error>>,

        #[snafu(implicit)]
        location: Location,
    },
}

type Result<T, E = Error> = std::result::Result<T, E>;

#[tokio::main]
async fn main() {
    tracing_subscriber::fmt::Subscriber::builder()
        .with_ansi(true)
        .with_max_level(Level::TRACE)
        .finish()
        .with(EnvFilter::from_default_env())
        .init();

    let config: Config = envious::Config::default()
        .without_prefix()
        .build_from_env()
        .expect("Could not deserialize config from env");

    let shared_config = aws_config::from_env()
        .endpoint_url(config.clone().aws_endpoint_url)
        .load()
        .await;
    let shared_config = Into::<aws_sdk_s3::config::Builder>::into(&shared_config)
        .force_path_style(true)
        .build();
    let s3_client = aws_sdk_s3::Client::from_conf(shared_config);

    let app_data = AppData { config, s3_client };

    let app = Router::new()
        .route("/health", get(|| async move { "UP" }))
        .route("/:host/*path", get(fetch_image))
        .layer(CompressionLayer::new())
        .with_state(app_data.clone());

    let listener = tokio::net::TcpListener::bind(format!(
        "{}:{}",
        app_data.config.address, app_data.config.port
    ))
    .await
    .unwrap_or_else(|_| {
        panic!(
            "unable to bind address {}:{}",
            app_data.config.address, app_data.config.port
        )
    });
    axum::serve(listener, app)
        .await
        .expect("unable to serve axum");
}

async fn fetch_image(
    State(app_data): State<AppData>,
    Path((host, full_path)): Path<(String, String)>,
    Query(parameters): Query<Parameters>,
) -> impl IntoResponse {
    if !app_data.config.allowed_hosts.contains(&host) {
        return (
            StatusCode::BAD_REQUEST,
            "The host is not in the allowed list!",
        )
            .into_response();
    }

    if let Some(size) = &parameters.size {
        if !app_data.config.allowed_sizes.contains(size) {
            return (
                StatusCode::BAD_REQUEST,
                "The size is not in the allowed list!",
            )
                .into_response();
        }
    }

    if let Some(crop) = &parameters.crop {
        if !app_data.config.allowed_crops.contains(crop) {
            return (
                StatusCode::BAD_REQUEST,
                "The crop is not in the allowed list!",
            )
                .into_response();
        }
    }

    if let Some(webp_encoder) = &parameters.webp_encoder {
        if !app_data.config.allow_webp {
            return (
                StatusCode::BAD_REQUEST,
                "WebP is not allowed in the current configuration",
            )
                .into_response();
        }

        if webp_encoder.quality < 0 {
            return (
                StatusCode::BAD_REQUEST,
                "WebP quality cannot be less than 0",
            )
                .into_response();
        }

        if webp_encoder.quality > 100 {
            return (
                StatusCode::BAD_REQUEST,
                "WebP quality cannot be greater than 100",
            )
                .into_response();
        }
    }

    match try_fetch_image(app_data, host, full_path, parameters).await {
        Ok(response) => response.into_response(),
        Err(error) => {
            let report = Report::from_error(error);
            log::error!("{:?}", report);
            (StatusCode::INTERNAL_SERVER_ERROR, report.to_string()).into_response()
        }
    }
}

async fn try_fetch_image(
    app_data: AppData,
    host: String,
    full_path: String,
    parameters: Parameters,
) -> Result<impl IntoResponse> {
    let file_name_raw = full_path.replace('/', "_");
    let mut file_name = file_name_raw.clone();

    if let Some(webp_encoder) = &parameters.webp_encoder {
        file_name = format!("{file_name}_{}.webp", webp_encoder.quality);
    };

    let file_path_raw = format!("{}/{host}/size/raw/crop/raw", app_data.config.images_path);
    let file_path = format!(
        "{}/{host}/size/{}/crop/{}",
        app_data.config.images_path,
        parameters
            .size
            .map_or("raw".to_string(), |size| format!("{size}").to_lowercase()),
        parameters
            .crop
            .clone()
            .map_or("raw".to_string(), |crop| format!(
                "{}_{}_{}_{}",
                crop.x, crop.y, crop.width, crop.height
            )
            .to_lowercase()),
    );

    let s3_file = get_object(&app_data, &file_path, &file_name).await?;

    let (format, data) = if let Some(data) = s3_file {
        let image = ImageReader::new(Cursor::new(&data)).with_guessed_format()?;
        let Some(format) = image.format() else {
            whatever!("unknown image format")
        };

        (format, data)
    } else {
        let s3_file = get_object(&app_data, &file_path_raw, &file_name_raw).await?;

        let data = if let Some(data) = s3_file {
            data
        } else {
            let response = reqwest::Client::new()
                .get(format!("https://{host}/{full_path}"))
                .send()
                .await;
            match response {
                Ok(response) => match response.status() {
                    reqwest::StatusCode::OK => {
                        let data = response.bytes().await?.to_vec();

                        put_object(&app_data, &file_path_raw, &file_name_raw, data.to_vec()).await?;

                        data
                    }
                    reqwest::StatusCode::NOT_FOUND => {
                        return Ok(StatusCode::NOT_FOUND.into_response());
                    }
                    status_code => whatever!("external provider had some problems, status_code: {status_code}, message: {:?}", response.text().await),
                },
                Err(error) => whatever!("unable to send request, received error: {error}"),
            }
        };

        let image = ImageReader::new(Cursor::new(&data)).with_guessed_format()?;
        let Some(format) = image.format() else {
            whatever!("unknown image format")
        };

        let mut dynamic_image = image.decode()?;

        if let Some(size) = parameters.size {
            dynamic_image = dynamic_image.resize(size, size, FilterType::Lanczos3);
        }

        if let Some(crop) = parameters.crop.clone() {
            dynamic_image = dynamic_image.crop(crop.x, crop.y, crop.width, crop.height);
        }

        if format == ImageFormat::Jpeg {
            dynamic_image = dynamic_image.into_rgb8().into();
        } else {
            dynamic_image = dynamic_image.into_rgba8().into();
        }

        // FIXME: Not supported right now
        let (format, data) = if let Some(_webp_parameter) = parameters.webp_encoder.clone() {
            let mut data = Vec::<u8>::new();

            #[allow(clippy::cast_precision_loss)]
            if let Err(err) = WebPEncoder::new_lossless(&mut data).encode(
                dynamic_image.as_bytes(),
                dynamic_image.width(),
                dynamic_image.height(),
                if format == ImageFormat::Jpeg {
                    ExtendedColorType::Rgb8
                } else {
                    ExtendedColorType::Rgba8
                },
            ) {
                eprintln!("{err:?}");
                let mut data = Vec::new();
                dynamic_image.write_to(&mut Cursor::new(&mut data), format)?;

                return Ok((
                    StatusCode::OK,
                    [
                        (header::CONTENT_TYPE, format.to_mime_type()),
                        (header::CONTENT_LENGTH, data.len().to_string().as_str()),
                    ],
                    data,
                )
                    .into_response());
            };

            (ImageFormat::WebP, data)
        } else {
            let mut data = Vec::new();
            dynamic_image.write_to(&mut Cursor::new(&mut data), format)?;

            (format, data)
        };

        put_object(&app_data, &file_path, &file_name, data.to_vec()).await?;

        (format, data)
    };

    let response = (
        StatusCode::OK,
        [
            (header::CONTENT_TYPE, format.to_mime_type()),
            (header::CONTENT_LENGTH, data.len().to_string().as_str()),
        ],
        data,
    )
        .into_response();

    Ok(response)
}

async fn get_object(
    app_data: &AppData,
    file_path: &String,
    file_name: &String,
) -> Result<Option<Vec<u8>>> {
    let key = format!("{file_path}/{file_name}");
    log::debug!("get_object {key}");

    let result = app_data
        .s3_client
        .get_object()
        .bucket("cdn")
        .key(&key)
        .send()
        .await;

    match result {
        Ok(object) => {
            log::debug!("object found for key {key}");
            let data = object
                .body
                .collect()
                .await
                .map(|data| Some(data.to_vec()))?;

            Ok(data)
        }
        Err(err) => match err.into_service_error() {
            GetObjectError::NoSuchKey(_) => {
                log::debug!("object not found for key {key}");

                Ok(None)
            }
            err => {
                log::error!("Error getting object: {err}");
                Err(err.into())
            }
        },
    }
}

async fn put_object(
    app_data: &AppData,
    file_path: &String,
    file_name: &String,
    data: Vec<u8>,
) -> Result<()> {
    let key = format!("{file_path}/{file_name}");
    log::debug!("put_object {key}");

    app_data
        .s3_client
        .put_object()
        .bucket("cdn")
        .key(&key)
        .body(data.clone().into())
        .send()
        .await
        .map_err(|error| Error::PutObject2 {
            key,
            size: size_of_val(&*data),
            message: error.message().unwrap_or_default().to_owned(),
        })?;

    Ok(())
}
