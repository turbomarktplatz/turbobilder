#!/usr/bin/env bash

if [ $# -eq 0 ]; then
  echo "Please specify tag"
  exit 1;
fi

docker \
  buildx \
  build \
  --push \
  --platform linux/amd64,linux/arm64 \
  -f CI/Dockerfile \
  -t registry.gitlab.com/turbomarktplatz/turbohinterseite:$1 \
  .
